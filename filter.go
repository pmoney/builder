package main

import (
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"time"
)

type Filter interface {
	Filter(path string, info os.FileInfo) bool
}

type NotHiddenFilter struct{}

func (n NotHiddenFilter) Filter(path string, info os.FileInfo) bool {
	return !strings.HasPrefix(path, ".")
}

type RegexFilter struct {
	pattern *regexp.Regexp
}

func (r RegexFilter) Filter(path string, info os.FileInfo) bool {
	return r.pattern.FindStringSubmatch(path) != nil
}

type ModifiedSinceFilter struct {
	t time.Time
}

func (m ModifiedSinceFilter) Filter(path string, info os.FileInfo) bool {
	return info.ModTime().After(m.t)
}

func filesMatching(root string, filters ...Filter) (paths []string) {
	filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return nil
		}
		for _, f := range filters {
			if !f.Filter(path, info) {
				return nil
			}
		}
		paths = append(paths, path)
		return nil
	})
	return
}
