package main

import (
	"os"
	"path/filepath"
	"regexp"
	"time"
)

// mostRecentlyModified returns the time at which the tree under root was most recently modified.
// This includes added/removed and modified files
func mostRecentlyModified(root string) (t time.Time) {
	info, err := os.Stat(root)
	if err != nil {
		return
	}
	if !info.IsDir() {
		return info.ModTime()
	}
	filepath.Walk(string(root), func(path string, info os.FileInfo, err error) error {
		if info.ModTime().After(t) {
			t = info.ModTime()
		}
		return nil
	})
	return
}

type buildTask struct {
	name    string
	src     string
	pattern *regexp.Regexp
	command string
	dest    string
}

func (b buildTask) RebuildIfNecessary() {
	t := mostRecentlyModified(b.dest)
	recentlyModified := ModifiedSinceFilter{t}
	notHidden := NotHiddenFilter{}
	matchingPattern := RegexFilter{b.pattern}
	if updatedFiles := filesMatching(b.src, notHidden, matchingPattern, recentlyModified); len(updatedFiles) > 0 {
		cmd := commandSyncer{b.command}
		cmd.Sync()
	} else {
		println("nothing to be done for " + b.name)
	}
}
