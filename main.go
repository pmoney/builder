package main

import (
	"fmt"
	"github.com/kylelemons/go-gypsy/yaml"
	"regexp"
)

func main() {
	if file, err := yaml.ReadFile("dist.yml"); err != nil {
		println("dist.yml not found, exiting...")
		return
	} else if builds, ok := file.Root.(yaml.Map); !ok {
		println("Root node must be a map of builds")
		return
	} else {
		for name, build := range builds {
			if buildConfig, ok := build.(yaml.Map); !ok {
				fmt.Printf("incorrect format for %s expected a map with src:,pattern:,command:,dest:\n", name)
				continue
			} else {
				task := buildTask{
					name:    name,
					src:     buildConfig["src"].(yaml.Scalar).String(),
					pattern: regexp.MustCompile(buildConfig["match"].(yaml.Scalar).String()),
					command: buildConfig["command"].(yaml.Scalar).String(),
					dest:    buildConfig["dest"].(yaml.Scalar).String(),
				}
				task.RebuildIfNecessary()
			}
		}
	}
}
