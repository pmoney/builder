package main

import (
	"fmt"
	"os/exec"
)

// A Syncer synchronises src => dest on the filesystem
// It doesn't need to check whether the sync needs to happen
type Syncer interface {
	Sync()
}

type commandSyncer struct {
	command string
}

func (c commandSyncer) Sync() {
	println(c.command)
	cmd := exec.Command("sh", "-c", c.command)
	if output, err := cmd.CombinedOutput(); err == nil {
		fmt.Printf("%s\n", string(output))
	}
}
